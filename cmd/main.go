package main

import (
	"flag"
	"fmt"
	"github.com/pterm/pterm"
	"github.com/riksa/horizontal-scroller/internal/git_rewriter"
	"github.com/riksa/horizontal-scroller/internal/rasterizer/github_rasterizer"
	"github.com/riksa/horizontal-scroller/internal/util"
	"image"
	"os"
	"path"
	"time"
)

func main() {
	clonePath, label := parseParameters()
	/*
		temp, err := os.MkdirTemp(os.TempDir(), "horizontal-scroller-template")
		defer os.RemoveAll(temp)

		if err != nil {
			exit(err.Error())
		}
		*clonePath = temp
	*/

	repo := cloneRepo(*clonePath)

	rasterizer, source := renderText(*label)

	img := resizeImage(rasterizer, source)

	filename := fmt.Sprintf("%s_scaled.png", *label)
	pterm.Info.Printfln("Saving file %s", filename)
	util.SavePng(img, filename)

	today := time.Now().UTC()
	imageWidth := img.Bounds().Dx()
	day := time.Hour * 24

	days := 367
	samples := make([]*git_rewriter.Sample, days)
	for delta := days - 1; delta >= 0; delta-- {
		targetDate := today.Add(day * time.Duration(-delta))
		coordinates := rasterizer.Coordinates(imageWidth, today, -delta)
		color := img.At(coordinates.X, coordinates.Y)
		commitCount := rasterizer.CommitCountNeeded(color)
		sample := git_rewriter.NewSample(targetDate, commitCount)
		samples[delta] = sample
	}

	commitCount, err := repo.MakeHistory(path.Join(*clonePath, "README.md"), samples)
	if err != nil {
		exit(err.Error())
	}
	pterm.Info.Printfln("wrote %s commits", pterm.LightRed(commitCount))

	// TODO: make a template repo with inital commit at least a year back
	// TODO: clone template
	// TODO: generate correct commits on it with MakeHistory()
	// TODO: profit

	os.Exit(0)
}

func checkCommitCount(rasterizer *github_rasterizer.GitHubRasterizer, img image.Image, repo *git_rewriter.Rewriter) bool {
	repoCount, err := repo.CommitCount()
	if err != nil {
		exit(err.Error())
	}
	pterm.Info.Printfln("Repo commits: %s", pterm.LightRed(repoCount))

	count := rasterizer.CommitCount(img)
	pterm.Info.Printfln("Rasterizer needs commits: %s", pterm.LightRed(count))
	return repoCount >= count
}

func resizeImage(rasterizer *github_rasterizer.GitHubRasterizer, source *image.RGBA) image.Image {
	pterm.Info.Printfln("Scaling image")
	img := rasterizer.Resize(source)
	srender, err := pterm.DefaultBulletList.WithItems([]pterm.BulletListItem{
		{Bullet: ">", Level: 1, Text: fmt.Sprintf("Image size %vx%v", img.Bounds().Dx(), img.Bounds().Dy())},
	}).Srender()
	if err != nil {
		exit(err.Error())
	}
	pterm.Info.Println(srender)
	return img
}

func renderText(label string) (*github_rasterizer.GitHubRasterizer, *image.RGBA) {
	file := fmt.Sprintf("%s.png", label)
	pterm.Info.Printfln("Saving to %s", pterm.LightRed(file))
	rasterizer := github_rasterizer.New()
	source := rasterizer.RasterizeText(label)
	srender, err := pterm.DefaultBulletList.WithItems([]pterm.BulletListItem{
		{Level: 1, Text: fmt.Sprintf("Image size %vx%v", source.Bounds().Dx(), source.Bounds().Dy())},
	}).Srender()
	if err != nil {
		exit(err.Error())
	}
	pterm.Info.Println(srender)

	err = util.SavePng(source, file)
	if err != nil {
		exit(err.Error())
	}
	return rasterizer, source
}

func cloneRepo(path string) *git_rewriter.Rewriter {
	template := "https://gitlab.com/riksa/horizontal-scroller-template.git"
	pterm.Info.Printfln("Cloning repository %s to %s", pterm.LightRed(template), pterm.LightRed(path))

	/*	_, err := os.Stat(path)
		if !os.IsNotExist(err) {
			exit(fmt.Sprintf("cannot clone to %s, file already exists", pterm.LightRed(path)))
		}*/

	progress := os.Stdout
	repo, err := git_rewriter.New(template, path, progress)
	if err != nil {
		exit(err.Error())
	}

	commitCount, err := repo.CommitCount()
	if err != nil {
		exit(err.Error())
	}

	srender, err := pterm.DefaultBulletList.WithItems([]pterm.BulletListItem{
		{Level: 1, Text: fmt.Sprintf("Commit count: %d", commitCount)},
	}).Srender()
	if err != nil {
		exit(err.Error())
	}
	pterm.Info.Println(srender)

	if err != nil {
		exit(err.Error())
	}
	return repo
}

func parseParameters() (*string, *string) {
	path := flag.String("r", "", "repository path")
	label := flag.String("l", "", "label")
	flag.Parse()

	if path == nil || *path == "" {
		exit("missing path -r")
	}

	if label == nil || *label == "" {
		exit("missing label -l")
	}
	return path, label
}

func exit(message string) {
	pterm.Error.Println(message)
	flag.Usage()
	os.Exit(-1)
}
