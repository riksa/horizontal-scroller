# Horizontal scroller for github/gitlab. 

[![coverage report](https://gitlab.com/riksa/horizontal-scroller/badges/master/coverage.svg)](https://gitlab.com/riksa/horizontal-scroller/-/commits/master)

Also, initial commit of this README fixes a test case by accident by increasing the commit count above 13. (I wish I had planned it, but alas, I just used > instead of >=)

https://gitlab.com/riksa/horizontal-scroller/-/jobs/1603099735

```
	Context("CommitCount", func() {
		It("should return commit count of repo", func() {
			// simplified for gitlab
			rewriter, err := New(thisRepo)
			Expect(err).ToNot(HaveOccurred())

			count, err := rewriter.CommitCount()
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(BeNumerically(">", 13))
		})
	})
```

```
    Git rewriter
    /go/src/gitlab.com/namespace/project/internal/git_rewriter/git_rewriter_test.go:11
      CommitCount
      /go/src/gitlab.com/namespace/project/internal/git_rewriter/git_rewriter_test.go:32
        should return commit count of repo [It]
        /go/src/gitlab.com/namespace/project/internal/git_rewriter/git_rewriter_test.go:33
        Expected
            <int>: 13
        to be >
            <int>: 13
        /go/src/gitlab.com/namespace/project/internal/git_rewriter/git_rewriter_test.go:40
```





