package git_rewriter

import (
	"github.com/go-git/go-git/v5"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"os"
	"path/filepath"
)

var _ = Describe("Git rewriter", func() {
	wd, err := os.Getwd()
	println(wd)

	thisRepo := filepath.Dir(filepath.Dir(wd))
	println(thisRepo)
	Expect(err).ToNot(HaveOccurred())

	Context("New", func() {
		It("should error on no repo", func() {
			rewriter, err := New("./does not exist", "")
			Expect(err).To(Equal(git.ErrRepositoryNotExists))
			Expect(rewriter).To(BeNil())
		})
		It("should open a repo", func() {
			// simplified for gitlab
			rewriter, err := New(thisRepo, "")
			Expect(err).ToNot(HaveOccurred())
			Expect(rewriter).ToNot(BeNil())
		})
	})
	Context("CommitCount", func() {
		It("should return commit count of repo", func() {
			// simplified for gitlab
			rewriter, err := New(thisRepo, "")
			Expect(err).ToNot(HaveOccurred())

			count, err := rewriter.CommitCount()
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(BeNumerically(">", 13))
		})
	})
})
