package git_rewriter

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestGitRewriter(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Git-rewriter Suite")
}
