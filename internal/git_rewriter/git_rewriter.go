package git_rewriter

import (
	"bytes"
	"fmt"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/protocol/packp/sideband"
	"github.com/pterm/pterm"
	"io"
	"io/ioutil"
	"os"
	"path"
	"time"
)

type Sample struct {
	date        time.Time
	commitCount int
}

func (s Sample) String() string {
	return fmt.Sprintf("%s commits: %d", s.date.Format("2006-01-02"), s.commitCount)
}

func NewSample(date time.Time, commitCount int) *Sample {
	return &Sample{
		date:        date,
		commitCount: commitCount,
	}
}

type Rewriter struct {
	path  string
	repo  *git.Repository
	Name  string
	Email string
}

func New(template, path string, progress sideband.Progress) (*Rewriter, error) {
	repo, err := git.PlainClone(path, false, &git.CloneOptions{
		URL:      template,
		Progress: progress,
	})
	if err != nil {
		return nil, err
	}
	return &Rewriter{
		Name:  "riksa",
		Email: "riksa@iki.fi",
		repo:  repo,
		path:  path,
	}, nil
}

func (r Rewriter) CommitCount() (int, error) {
	iter, err := r.repo.CommitObjects()
	if err != nil {
		return -1, err
	}

	c := 0
	f := func(commit *object.Commit) error {
		c++
		return nil
	}

	err = iter.ForEach(f)
	return c, err
}

func (r Rewriter) MakeHistory(templateFile string, samples []*Sample) (int, error) {
	w, err := r.repo.Worktree()
	if err != nil {
		return -1, err
	}

	template, err := ioutil.ReadFile(templateFile)
	if err != nil {
		return -1, err
	}
	reader := bytes.NewReader(template)

	filename := path.Join(r.path, "REWRITE.md")
	pterm.Info.Printfln("Creating commits to: %s as %s <%s>", filename, pterm.LightRed(r.Name), pterm.LightRed(r.Email))
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0755)
	defer file.Close()

	commitCount := 0
	for i, c := range samples {
		for x := 0; x < c.commitCount; x++ {
			run, err := readWithReset(reader)
			if err != nil {
				return commitCount, err
			}

			s := string(run)
			//			pterm.Info.Printfln("Wrote", s)
			_, err = file.WriteString(s)

			if err != nil {
				return commitCount, err
			}

			w.Add(".")
			msg := fmt.Sprintf("chore: rewrite #%d %s", i, c.String())
			pterm.Info.Printfln(msg)
			_, err = w.Commit(msg, &git.CommitOptions{
				Author: &object.Signature{
					Name:  r.Name,
					Email: r.Email,
					When:  c.date,
				},
				All: true,
			})
			commitCount++
		}
	}

	return commitCount, nil
}

func readWithReset(reader *bytes.Reader) (rune, error) {
	ch, _, err := reader.ReadRune()
	if err != nil {
		// reset and try again
		pterm.Info.Printfln("Out of bytes on template, resetting")
		_, err := reader.Seek(0, io.SeekStart)
		if err != nil {
			return 0, err
		}
		ch, _, err = reader.ReadRune()
		if err != nil {
			return 0, err
		}
	}
	return ch, nil
}
