package util

import (
	"image"
	"image/png"
	"os"
)

func SavePng(image image.Image, filename string) error {
	f, err := os.Create(filename)
	if err != nil {
		panic(err)
	}

	defer f.Close()
	return png.Encode(f, image)
}

func LoadPng(filename string) (image.Image, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	return png.Decode(file)
}
