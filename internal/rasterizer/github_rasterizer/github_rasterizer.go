package github_rasterizer

import (
	"github.com/nfnt/resize"
	"github.com/pterm/pterm"
	"github.com/riksa/horizontal-scroller/internal/rasterizer"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	"golang.org/x/image/math/fixed"
	"image"
	"image/color"
	"math"
	"time"
)

var _ rasterizer.Rasterizer = GitHubRasterizer{}

type GitHubRasterizer struct {
	imageSize image.Point
}

func New() *GitHubRasterizer {
	return &GitHubRasterizer{
		imageSize: image.Point{X: -1, Y: 7},
	}
}

func (g GitHubRasterizer) RasterizeText(text string) *image.RGBA {
	imageWidth := len(text) * 7
	img := image.NewRGBA(image.Rect(0, 0, imageWidth, 2*7))
	g.addLabel(img, 0, 11, text)

	return img
}

func (g GitHubRasterizer) addLabel(img *image.RGBA, x, y int, text string) {
	col := color.RGBA{G: 255, A: 255}
	point := fixed.Point26_6{X: fixed.Int26_6(x * 64), Y: fixed.Int26_6(y * 64)}

	d := &font.Drawer{
		Dst:  img,
		Src:  image.NewUniform(col),
		Face: basicfont.Face7x13,
		Dot:  point,
	}
	d.DrawString(text)
}

func (g GitHubRasterizer) Coordinates(imageWidth int, today time.Time, deltaDays int) image.Point {
	oneDay := 24 * time.Hour
	target := today.Add(oneDay * time.Duration(deltaDays))
	sundayThisWeek := today.Add(time.Duration(today.Weekday()) * -oneDay)
	sundayTargetWeek := target.Add(time.Duration(target.Weekday()) * -oneDay)
	diffWeeks := sundayThisWeek.Sub(sundayTargetWeek).Hours() / (24 * 7)
	diffFullWeeks := int(math.Ceil(diffWeeks))
	return image.Point{X: imageWidth - 1 - diffFullWeeks, Y: int(target.Weekday())}
}

func (_ GitHubRasterizer) Resize(source *image.RGBA) image.Image {
	ss := source.Bounds()
	ts := ss.Bounds()
	ts.Min.Y += 2
	ts.Max.Y -= 3
	i := source.SubImage(ts)

	bounds := i.Bounds()
	return resize.Resize(uint(bounds.Dx()), 7, i, resize.Lanczos3)
	/*
		rect := source.Rect
		sourceWidth, sourceHeight := rect.Dx(), rect.Dy()

		targetWidth, targetHeight := sourceWidth>>1, sourceHeight>>1

		source.re

		sampled := image.NewRGBA(image.Rect(0, 0, targetWidth, targetHeight))

		samples := make([]uint32, 9)
		for targetY := 0; targetY < targetHeight; targetY++ {
			for targetX := 0; targetX < targetWidth; targetX++ {
				sourceX, sourceY := targetX<<1, targetY<<1

				_, samples[0], _, _ = source.At(sourceX, sourceY-1).RGBA()
				_, samples[1], _, _ = source.At(sourceX, sourceY).RGBA()
				_, samples[2], _, _ = source.At(sourceX, sourceY+1).RGBA()
				_, samples[3], _, _ = source.At(sourceX+1, sourceY-1).RGBA()
				_, samples[4], _, _ = source.At(sourceX+1, sourceY).RGBA()
				_, samples[5], _, _ = source.At(sourceX+1, sourceY+1).RGBA()
				_, samples[6], _, _ = source.At(sourceX-1, sourceY-1).RGBA()
				_, samples[7], _, _ = source.At(sourceX-1, sourceY).RGBA()
				_, samples[8], _, _ = source.At(sourceX-1, sourceY+1).RGBA()

				r := uint8(sourceX*8) * 0
				g := uint8(0)
				b := uint8(sourceY*10) * 0
				for _, s := range samples {
					if s > 110 {
						g += 25
					}
				}
				g= uint8(samples[0] * 200)
				c := color.RGBA{R: r, G: g, B: b, A: 255}
				sampled.SetRGBA(targetX, targetY, c)
			}
		}
		return sampled
	*/
}

func (g GitHubRasterizer) CommitCount(img image.Image) int {
	source := image.NewRGBA(img.Bounds())
	rect := img.Bounds()
	for y := rect.Min.Y; y < rect.Max.Y; y++ {
		for x := rect.Min.X; x < rect.Max.X; x++ {
			c := img.At(x, y)
			needed := g.CommitCountNeeded(c)
			switch needed {
			case 4:
				pterm.Print(pterm.Green("*"))
			default:
				pterm.Print(pterm.Green(" "))
			}
			source.Set(x, y, c)
		}
		pterm.Println()
	}
	r := image.Rect(source.Bounds().Dx()-52, 0, source.Bounds().Dx(), 14)
	s := source.SubImage(r)
	bounds := s.Bounds()

	commitCount := 0
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			commitCount += g.CommitCountNeeded(s.At(x, y))
		}
	}
	return commitCount
}

func (g GitHubRasterizer) CommitCountNeeded(c color.Color) int {
	_, green, _, a := c.RGBA()
	if a == 0 {
		return 0
	}
	scaled := green >> 8
	pterm.Info.Printfln("a:%v g:%v scaled:%v", a, green, scaled)
	if scaled > 200 {
		return 4
	} else if scaled > 150 {
		return 3
	} else if scaled > 100 {
		return 2
	} else if scaled > 50 {
		return 1
	}
	return 0

}
