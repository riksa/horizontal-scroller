package github_rasterizer

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestGithubRasterizer(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Github rasterizer Suite")
}
