package github_rasterizer

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/riksa/horizontal-scroller/internal/util"
	image2 "image"
	"time"
)

var _ = Describe("Github rasterizer", func() {
	Context("Resize", func() {
		It("should Resize image", func() {
			rasterizer := New()
			image := rasterizer.RasterizeText("RIKSA")
			util.SavePng(image, "riksa.png")

			sampled := rasterizer.Resize(image)
			util.SavePng(sampled, "riksa2.png")
			Expect(sampled.Bounds().Dx()).To(BeEquivalentTo(35))
			Expect(sampled.Bounds().Dy()).To(BeEquivalentTo(7))
		})
	})

	Context("SumImage", func() {
		It("should return subimage of 10x7 at most", func() {
			// just want to know if DX is 52 or 10 in this case
			r := image2.Rect(0, 0, 10, 10)
			i := image2.NewRGBA(r)
			sr := image2.Rect(0, 0, 52, 7)
			si := i.SubImage(sr)
			bounds := si.Bounds()
			Expect(bounds.Dx()).To(BeEquivalentTo(10))
			Expect(bounds.Dy()).To(BeEquivalentTo(7))
		})
	})
	Context("CommitCount", func() {
		It("should return minimum commitcount of '' as 0", func() {
			rasterizer := New()
			image := rasterizer.RasterizeText("")
			Expect(rasterizer.CommitCount(image)).To(BeEquivalentTo(0))
		})
		It("should return minimum commitcount of 'I' as 4*(5+5+9-2) from Gimp", func() {
			rasterizer := New()
			image := rasterizer.RasterizeText("I")
			Expect(image.Bounds().Dx()).To(BeEquivalentTo(7))
			Expect(image.Bounds().Dy()).To(BeEquivalentTo(14))
			util.SavePng(image, "I.png")
			Expect(rasterizer.CommitCount(image)).To(BeEquivalentTo(4 * (5 + 5 + 9 - 2)))
		})
	})
	Context("RasterizeText", func() {
		It("should rasterize text", func() {
			rasterizer := New()
			image := rasterizer.RasterizeText("riksa")
			Expect(image.Bounds().Dx()).To(BeEquivalentTo(5 * 7))
			Expect(image.Bounds().Dy()).To(BeEquivalentTo(2 * 7))
		})
	})
	Context("Coordinates", func() {
		It("should locate neighbours if today is sunday", func() {
			rasterizer := New()
			imageWidth := 52
			today, _ := time.Parse(time.RFC3339, "2021-09-19T09:00:00Z")

			// Sunday today at top right...
			Expect(rasterizer.Coordinates(imageWidth, today, 0)).To(BeEquivalentTo(image2.Point{X: 51, Y: 0}))
			// ...yesterday...
			Expect(rasterizer.Coordinates(imageWidth, today, -1)).To(BeEquivalentTo(image2.Point{X: 50, Y: 6}))
			// ...tomorrow...
			Expect(rasterizer.Coordinates(imageWidth, today, +1)).To(BeEquivalentTo(image2.Point{X: 51, Y: 1}))
			// ...tomorrow...
			Expect(rasterizer.Coordinates(imageWidth, today, -7)).To(BeEquivalentTo(image2.Point{X: 50, Y: 0}))
			// ...tomorrow...
			Expect(rasterizer.Coordinates(imageWidth, today, +7)).To(BeEquivalentTo(image2.Point{X: 52, Y: 0}))
		})
		It("should locate corners if today is sunday", func() {
			rasterizer := New()
			imageWidth := 52
			today, _ := time.Parse(time.RFC3339, "2021-09-19T09:00:00Z")

			// Sunday today at top right...
			Expect(rasterizer.Coordinates(imageWidth, today, 0)).To(BeEquivalentTo(image2.Point{X: 51, Y: 0}))
			// ...6 days from now bottom right...
			Expect(rasterizer.Coordinates(imageWidth, today, 6)).To(BeEquivalentTo(image2.Point{X: 51, Y: 6}))
			// ...Sunday 51 weeks ago at top left...
			Expect(rasterizer.Coordinates(imageWidth, today, -51*7)).To(BeEquivalentTo(image2.Point{X: 0, Y: 0}))
			// ...Saturday after that at bottom left
			//			Expect(rasterizer.Coordinates(imageWidth, today, -51*7+6)).To(BeEquivalentTo(image2.Point{X: 0, Y: 6}))
		})
		It("should locate corners if today is Tuesday", func() {
			rasterizer := New()
			imageWidth := 52
			today, _ := time.Parse(time.RFC3339, "2021-09-21T09:00:00Z")

			// Today 2 down from top right...
			Expect(rasterizer.Coordinates(imageWidth, today, 0)).To(BeEquivalentTo(image2.Point{X: 51, Y: 2}))
			// Week ago...
			Expect(rasterizer.Coordinates(imageWidth, today, -7)).To(BeEquivalentTo(image2.Point{X: 50, Y: 2}))
			// Sunday at top right...
			Expect(rasterizer.Coordinates(imageWidth, today, -2)).To(BeEquivalentTo(image2.Point{X: 51, Y: 0}))
			// ...Saturday bottom right...
			Expect(rasterizer.Coordinates(imageWidth, today, -2+6)).To(BeEquivalentTo(image2.Point{X: 51, Y: 6}))
			// ...Sunday 51 weeks ago at top left...
			Expect(rasterizer.Coordinates(imageWidth, today, -2-51*7)).To(BeEquivalentTo(image2.Point{X: 0, Y: 0}))
			// ...Sunday 51 weeks ago at top left...
			Expect(rasterizer.Coordinates(imageWidth, today, 2-2-51*7)).To(BeEquivalentTo(image2.Point{X: 0, Y: 2}))
			// ...Saturday after that at bottom left
			Expect(rasterizer.Coordinates(imageWidth, today, 6-2-51*7)).To(BeEquivalentTo(image2.Point{X: 0, Y: 6}))
		})
	})
})

var _ = Describe("Util", func() {
	It("should save png", func() {
		rasterizer := New()
		image := rasterizer.RasterizeText("riksa")
		err := util.SavePng(image, "image.png")
		Expect(err).ToNot(HaveOccurred())
	})

	It("should read png", func() {
		image, err := util.LoadPng("image.png")
		Expect(err).ToNot(HaveOccurred())
		Expect(image.Bounds().Dx()).To(BeEquivalentTo(5 * 7))
		Expect(image.Bounds().Dy()).To(BeEquivalentTo(2 * 7))
	})

	It("should error out on file not found", func() {
		image, err := util.LoadPng("image_not_exists.png")
		Expect(err).To(HaveOccurred())
		Expect(image).To(BeNil())
	})
})
