package rasterizer

import (
	"image"
	"time"
)

type Rasterizer interface {
	RasterizeText(text string) *image.RGBA
	Coordinates(imageWidth int, today time.Time, deltaDays int) image.Point
	Resize(source *image.RGBA) image.Image
}
